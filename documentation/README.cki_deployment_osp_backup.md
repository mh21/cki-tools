# `shell-scripts/cki_deployment_osp_backup.sh`

Backup an OpenStack instance by creating snapshots.

```shell
cki_deployment_osp_backup.sh <name> <group> <count>
```

For each instance `<name>`, the last `<count>` images per backup group `<group>`
are kept.

The following `crontab` schedule would perform daily, weekly, monthly and
yearly backups:

```plain
@daily   cki_deployment_osp_backup.sh instance-name daily    7
@weekly  cki_deployment_osp_backup.sh instance-name weekly   4
@monthly cki_deployment_osp_backup.sh instance-name monthly 12
@yearly  cki_deployment_osp_backup.sh instance-name yearly   2
```

## Environment variables

| Field                    | Type   | Required | Description                     |
|--------------------------|--------|----------|---------------------------------|
| `OS_PROJECT_NAME`        | string | yes      | OpenStack project name          |
| `OS_PROJECT_DOMAIN_NAME` | string | yes      | OpenStack project domain name   |
| `OS_AUTH_URL`            | string | yes      | OpenStack Keystone endpoint URL |
| `OS_USERNAME`            | string | yes      | OpenStack user name             |
| `OS_USER_DOMAIN_NAME`    | string | yes      | OpenStack user domain name      |
| `OS_PASSWORD`            | string | yes      | OpenStack password              |
