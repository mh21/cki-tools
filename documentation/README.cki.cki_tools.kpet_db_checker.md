# `cki.cki_tools.kpet_db_checker` - check kpet db templates

A tool to check that tasks have mandatory parameters.

```text
python3  -m cki.cki_tools.kpet_db_checker  --help
usage: kpet_db_checker.py [-h] --input-file INPUT_FILE --param name

Check template params

optional arguments:
  -h, --help            show this help message and exit
  --input-file INPUT_FILE
                        File to search task attributes
  --param name          Param name to search in the task, it can be used several
                        times
```

The `input file` parameter is the file that contains tasks. This file should not be an XML file.

The `param` parameter is a string, and it's the parameter's name. This argument can be used
several times. When multiple parameters are set, all of them must be present in the task.

The script will extract chrunkes, every chrunk represents a task, and it will search all parameters
in that task.

After that, the script will show a summary existing `0` if all tasks contain all parameters.
Otherwise will return `1`.

Examples:

```shell
$ python3  -m cki.cki_tools.kpet_db_checker  --input-file beaker.xml.j2 --param CKI_IS_TEST
Searching CKI_IS_TEST param(s) in beaker.xml.j2
Tasks: 16, errors: 0
```

```shell
$ python3  -m cki.cki_tools.kpet_db_checker  --input-file beaker.xml.j2 --param CKI_IS_TEST --param CKI_ID
Searching CKI_IS_TEST, CKI_ID param(s) in beaker.xml.j2
Tasks: 16, errors: 9
Param(s) not found: CKI_ID in a task (lines 4 - 10)
Param(s) not found: CKI_ID in a task (lines 47 - 53)
Param(s) not found: CKI_ID in a task (lines 290 - 299)
Param(s) not found: CKI_ID in a task (lines 324 - 329)
Param(s) not found: CKI_ID in a task (lines 332 - 338)
Param(s) not found: CKI_ID in a task (lines 342 - 348)
Param(s) not found: CKI_ID in a task (lines 383 - 393)
Param(s) not found: CKI_ID in a task (lines 405 - 411)
Param(s) not found: CKI_ID in a task (lines 430 - 436)
```
