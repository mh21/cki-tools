# `cki.deployment_tools.grafana`

```shell
python3 -m cki.deployment_tools.grafana download|upload [--path PATH]
```

Tool to backup and restore Grafana data. Download configuration into locally stored
JSON files, which can be later be modified and re uploaded.

- `download` option uses Grafana API to download datasources, notification channels and
dashboards and saves them to PATH.

- `upload` takes the json files stored in PATH and restores them to the Grafana server.

`GRAFANA_URL` and `GRAFANA_TOKEN` environment variables need to be configured.
Follow the instructions on the [Grafana Docs] to learn how to get an API Token.

[Grafana Docs]: https://grafana.com/docs/grafana/latest/http_api/auth/#create-api-token
