# `cki.kcidb.datawarehouse_submitter` - KCIDB submitter

The **datawarehouse_submitter** listens to a message _queue_,
which receives a notification every time a job successfully passes through the [pipeline-herder].
It then retrieves the corresponding results and submits them to the [DataWarehouse][dw].

The results are stored in an artifact called `kcidb_all.json` using the
[KCIDB format][kcidb] schemed data.

It can also be used to manually retrieve and optionally submit the results from a _single_ job
or every job in a _single_ pipeline.

[kcidb]: https://github.com/kernelci/kcidb-io
[pipeline-herder]: https://gitlab.com/cki-project/pipeline-herder
[dw]: https://datawarehouse.cki-project.org/

## SYNOPSIS

```python
python3 -m cki.kcidb.datawarehouse_submitter [-h]
    [--push]
    [--datawarehouse-url  DATAWAREHOUSE_URL]
    [--datawarehouse-token  DATAWAREHOUSE_TOKEN]

    [--output-file OUTPUT_FILE]

    [--output-stdout]
    {queue,single} ...
```

- `--push`: Push output data to Datawarehouse. Defaults to True _on production_.
- `--datawarehouse-url DATAWAREHOUSE_URL`: URL to the Datawarehouse instance. Defaults to env DATAWAREHOUSE_URL.
- `--datawarehouse-token DATAWAREHOUSE_TOKEN`: Token for the Datawarehouse instance.
Defaults to env DATAWAREHOUSE_URL.
- `--output-file OUTPUT_FILE`: Save output to a file.
- `--output-stdout`: Echo output to stdout. Defaults to False _on production_.

## Single

Parse a single job or pipeline:

```python
python3 -m cki.kcidb.datawarehouse_submitter single [-h]
    {pipeline,job} gitlab_url project id

```

- `{pipeline,job}`: choose between pipeline or job
- `gitlab_url`: Gitlab host URL.
- `project`: Gitlab project. Id or url-encoded path.
- `id`: Gitlab pipeline or job id.

## Queue

Monitor queue for new finished jobs:

```python
python3 -m cki.kcidb.datawarehouse_submitter queue [-h]
    [--rabbitmq-exchange RABBITMQ_EXCHANGE]
    [--rabbitmq-queue RABBITMQ_QUEUE]
    [--rabbitmq-host RABBITMQ_HOST]
    [--rabbitmq-port RABBITMQ_PORT]
    [--rabbitmq-user RABBITMQ_USER]
    [--rabbitmq-password RABBITMQ_PASSWORD]
```

## Environment variables

| Name                            | Secret | Description                                            |
|---------------------------------|--------|--------------------------------------------------------|
| `GITLAB_TOKEN`                  | yes    | GitLab private token with access to the Git repo       |
| `DATAWAREHOUSE_URL`             | no     | URL to DataWarehouse.                                  |
| `DATAWAREHOUSE_TOKEN_SUBMITTER` | yes    | Token for DataWarehouse.                               |
| `RABBITMQ_HOST`                 | no     | AMQP host                                              |
| `RABBITMQ_PORT`                 | no     | AMQP port, TLS is used for port 443 (defaults to 5672) |
| `RABBITMQ_USER`                 | no     | AMQP user                                              |
| `RABBITMQ_PASSWORD`             | yes    | AMQP password                                          |
| `RABBITMQ_EXCHANGE`             | no     | AMQP exchange for the posted messages                  |
| `RABBITMQ_QUEUE`                | no     | AMQP queue to post messages                            |
