#!/usr/bin/python3
"""Datawarehouse failure triager."""
import argparse
import sys

from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
from cki_lib.logger import get_logger
import sentry_sdk

from cki.triager import settings
from cki.triager import triager

LOGGER = get_logger('cki.triager')
IS_PRODUCTION = misc.is_production()
MESSAGES_TRIAGE = ('new', 'updated', 'needs_triage')


def callback(dry_run, body=None, ack_fn=None, **_):
    """Handle a single message."""
    obj = body['object']
    obj_type = body['object_type']
    status = body['status']
    msg_misc = body.get('misc', {})
    LOGGER.info('Got message for (%s) %s id=%s misc=%s',
                status, obj_type, obj['id'], msg_misc)
    if status in MESSAGES_TRIAGE:
        triager.Triager(dry_run).check(obj_type, obj, msg_misc)

    if not dry_run:
        ack_fn()


def triage_queue(dry_run=False):
    """Triage elements received using the message queue."""
    LOGGER.info("Running checks on queue items.")
    queue = messagequeue.MessageQueue()
    queue.consume_messages(settings.EXCHANGE_NAME, ['#'],
                           lambda **kwargs: callback(dry_run, **kwargs),
                           queue_name=settings.MESSAGE_QUEUE_NAME,
                           manual_ack=True)


def triage_single(obj_type, obj_id, regex_id=None, dry_run=False):
    """Triage a single object."""
    triager.Triager(dry_run).check(obj_type, obj_id,
                                   {'issueregex_ids': [regex_id]} if regex_id else None)


def parse_args(args):
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--dry-run', action='store_true', default=(not IS_PRODUCTION),
                        help='Just look for issues. Do not create failure reports.')
    parser.add_argument('--no-dry-run', dest='dry_run', action='store_false')
    subparsers = parser.add_subparsers(dest='strategy')
    # Queue parsing subperser.
    subparsers.add_parser('queue', help='Triage objects by subscribing to a RabbitMQ queue')
    # Single object parsing subperser.
    parser_single = subparsers.add_parser('single', help='Triage a single kcidb object')
    parser_single.add_argument(
        'type', choices=['checkout', 'build', 'test', 'testresult'], help='Kind of stuff to check'
    )
    parser_single.add_argument(
        'id', help='Id or iid of the object to check'
    )
    parser_single.add_argument('--regex-id', help='Only check specified issue regex')

    return parser.parse_args(args)


if __name__ == '__main__':
    misc.sentry_init(sentry_sdk)
    arguments = parse_args(sys.argv[1:])

    metrics.prometheus_init()

    if not arguments.strategy or arguments.strategy == 'queue':
        triage_queue(arguments.dry_run)
    elif arguments.strategy == 'single':
        triage_single(arguments.type, arguments.id, arguments.regex_id, arguments.dry_run)
