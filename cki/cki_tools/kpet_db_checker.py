"""Check param(s) in tasks given a file."""

import argparse
from dataclasses import dataclass
import re
import sys
import typing

start_pattern = re.compile(r'^<task')
end_pattern = re.compile(r'^</task>')


@dataclass
class Block:
    """Dataclass for a block."""

    start: int
    end: int
    content: str


def search_param(block: str, name: str) -> bool:
    """Search param in a given block."""
    re_param = (fr".*(<param(\s+name=\"{name}\"\s+value=\".*\""
                fr"|\s+value=\".*\"\s+name=\"{name}\")\s*\/\>)")
    return re.search(re_param, block, re.MULTILINE) is not None


def get_tasks(file: typing.Iterable[typing.Any]) -> typing.Iterable[Block]:
    """Iterate into the file and get the blocks."""
    blocks: typing.List[Block] = []
    block_start: int = -1
    block_end: int = -1
    current_block: str = ''

    for line_num, line in enumerate(file, start=1):
        _line = line.lstrip()

        if start_pattern.match(_line):
            block_start = line_num
            current_block = ''

        elif end_pattern.match(_line):
            block_end = line_num

        current_block += line

        if block_start != -1 and block_end != -1:
            blocks.append(Block(block_start, block_end, current_block))
            block_start = -1
            block_end = -1

    return blocks


def main(args: typing.Optional[typing.List[str]] = None) -> int:
    """Run the script as cli."""
    output = 0
    errors = []
    successes = []
    parser = argparse.ArgumentParser(description="KPET DB Checker")
    parser.add_argument('--input-file', required=True,
                        type=argparse.FileType('r', encoding='utf-8'),
                        help='File to search task attributes')
    parser.add_argument('--param', required=True, type=str, action='append', dest='params',
                        metavar='name',
                        help='Param name to search in the task, it can be used several times')
    parser_args = parser.parse_args(args)
    tasks = get_tasks(parser_args.input_file)
    print(f'Searching {", ".join(parser_args.params)} param(s) in {parser_args.input_file.name}')
    for task in tasks:
        unfound_strings = []
        for param in parser_args.params:
            if not search_param(task.content, param):
                unfound_strings.append(param)

        if unfound_strings:
            output = 1
            errors.append({'params': unfound_strings, 'start': task.start, 'end': task.end})
        else:
            successes.append({'start': task.start, 'end': task.end})

    print(f'Tasks: {len(tasks)}, errors: {len(errors)}')  # type: ignore
    for error in errors:
        print(f'Param(s) not found: {", ".join(error["params"])} in a task '  # type: ignore
              f'(lines {error["start"]} - {error["end"]})')

    return output


if __name__ == '__main__':  # pragma: no cover
    sys.exit(main(sys.argv[1:]))
