#!/bin/bash

set -Eeuo pipefail
shopt -s inherit_errexit

export START_PYTHON_SERVICE_METRICS="cki.cki_tools.service_metrics"
export LOG_NAME="service_metrics"

exec cki_entrypoint.sh
