"""Route webhook messages to an amqp queue (Flask version)."""
from cki_lib import misc
import flask
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from . import receiver

app = flask.Flask(__name__)
misc.sentry_init(sentry_sdk, integrations=[FlaskIntegration()])

receiver.TRY_ENDLESSLY = False


def _flask_handler(method):
    status_code, message = method(flask.request.headers,
                                  flask.request.get_data(),
                                  request_args=flask.request.args)
    if status_code != 200:
        flask.abort(status_code, message)
    return message


@app.route('/', methods=['POST'])
def gitlab_webhook():
    """Process a webhook."""
    return _flask_handler(receiver.gitlab_handler)


@app.route('/sentry', methods=['POST'])
def sentry_webhook():
    """Process a webhook."""
    return _flask_handler(receiver.sentry_handler)


@app.route('/jira', methods=['POST'])
def jira_webhook():
    """Process a webhook."""
    return _flask_handler(receiver.jira_handler)
