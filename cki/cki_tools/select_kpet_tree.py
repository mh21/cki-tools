"""Determine the correct kpet tree for a tree family and the kernel NVR."""
import argparse
import re
import sys

from cki_lib import misc
import sentry_sdk

mapping = {
    r'rhel6': [
        (r'\.el6', 'rhel610-z'),                                 # supported zstream 6.10
    ],
    r'rhel7': [
        (r'3\.10\.0-693\b.*\.el7', 'rhel74-z'),                  # supported zstream 7.4
        (r'3\.10\.0-957\b.*\.el7', 'rhel76-z'),                  # supported zstream 7.6
        (r'3\.10\.0-1062\b.*\.el7', 'rhel77-z'),                 # supported zstream 7.7
        (r'3\.10\.0-1160\b.*\.el7', 'rhel79-z'),                 # supported zstream 7.9
    ],
    r'rhel7-rt': [
        (r'3\.10\.0-957\b.*\.el7', 'rhel76-z-rt'),               # supported zstream 7.6
        (r'3\.10\.0-1160\b.*\.el7', 'rhel79-z-rt'),              # supported zstream 7.9
    ],
    r'rhel8(?P<suffix>-rt|)': [
        (r'\.el8_(?P<minor>[124678])', 'rhel8{minor}-z{suffix}'),  # supported zstream 8.[124678]
        (r'\.el8_[9]', 'rhel8{suffix}'),                         # non-existing zstream -> ystream
        (r'\.el8(?!_)', 'rhel8{suffix}'),                        # ystream
    ],
    r'rhel9(?P<suffix>-rt|)': [
        (r'\.el9_(?P<minor>[012])', 'rhel9{minor}-z{suffix}'),   # supported zstream 9.[012]
        (r'\.el9_[3456789]', 'rhel9{suffix}'),                   # non-existing zstream -> ystream
        (r'\.el9(?!_)', 'rhel9{suffix}')                         # ystream
    ],
}


def map_nvr(tree_family, nvr):
    """Return the kpet tree name for a tree family and an NVR."""
    for family_regex, nvr_regexes in mapping.items():
        family_match = re.fullmatch(family_regex, tree_family)
        if not family_match:
            continue
        for nvr_regex, tree_name in nvr_regexes:
            nvr_regex = nvr_regex.format_map(family_match.groupdict())
            nvr_match = re.search(nvr_regex, nvr)
            if nvr_match:
                return tree_name.format_map({**family_match.groupdict(),
                                             **nvr_match.groupdict()})

        # Raise exception here, so we can fixup tree mapping and restart the pipeline.
        raise RuntimeError('kpet tree name for this NVR could not be matched')
    return tree_family


def main(args):
    """Run the main command line interface."""
    misc.sentry_init(sentry_sdk)
    parser = argparse.ArgumentParser()
    parser.add_argument('tree_family', help='kpet tree family')
    parser.add_argument('positional_nvr', help='kernel Name-Version-Release information')
    parser.add_argument('--package-name', help='kernel binary RPM base name')
    parser.add_argument('--kernel-version', help='kernel Version-Release information')
    parser.add_argument('--nvr-tree-mapping', help='nvr-tree mapping data as YAML')
    parser.add_argument('--nvr-tree-mapping-file', help='File with nvr-tree mapping data')
    parsed_args = parser.parse_args(args)

    print(map_nvr(parsed_args.tree_family, parsed_args.positional_nvr))


if __name__ == '__main__':
    main(sys.argv[1:])
