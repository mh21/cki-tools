␛[0Ksection_start:1670841260:step_script
␛[0K␛[0K␛[36;1mExecuting "step_script" stage of the job script␛[0;m␛[0;m
␛[0KUsing docker image sha256:6c0d561527f7508104593289d9ae92554db1c90eba8f080922e93f4167ccc06b for quay.io/cki/python:production with digest quay.io/cki/python@sha256:2b5eaf25cc8bea3f9ed9a41cd774d46848f8e3a4e28c8251d88d659f86bc76c4 ...␛[0;m
␛[32;1m$ # Create a precollapsed section to hide the usually uninteresting before-script # collapsed multi-line command␛[0;m
␛[0Ksection_start:1670841260:before_script[collapsed=true]
␛[0KExport needed functions and variables
␛[32;1m$ set -euo pipefail␛[0;m
␛[32;1m$ # Export general pipeline functions # collapsed multi-line command␛[0;m
␛[32;1m$ # Export AWS pipeline functions # collapsed multi-line command␛[0;m
␛[32;1m$ # Export KCIDB pipeline functions # collapsed multi-line command␛[0;m
␛[32;1m$ # Export functions used only by prepare # collapsed multi-line command␛[0;m
␛[32;1m$ # Export variables that we need to build kcidb schema # collapsed multi-line command␛[0;m
␛[32;1m$ # Set up the path for locally installed python executables and ccache. # collapsed multi-line command␛[0;m
🏠 Home directory is set: /tmp
📦 Container image:
  cki-project/containers/python:20220831.1@git-aa9de102
🏗️ API URLs:
  Pipeline: https://gitlab.com/api/v4/projects/18194050/pipelines/719736760
  Variables: https://gitlab.com/api/v4/projects/18194050/pipelines/719736760/variables
  Job: https://gitlab.com/api/v4/projects/18194050/jobs/3458517826
💻 Determining CPU and job count
  CPUs: 2
  job count: 3
␛[1;32m🗄️ Downloading artifacts from S3 bucket...␛[0m
Downloading artifacts for '719736760/prepare python'
  Previous jobs: 3458517810
Downloading artifacts from 719736760/prepare python/3458517810
Unable to locate credentials. You can configure credentials by running "aws configure".
section_end:1670841265:step_script
␛[0Ksection_start:1670841265:after_script
␛[0K␛[0K␛[36;1mRunning after_script␛[0;m␛[0;m
␛[32;1mRunning after script...␛[0;m
