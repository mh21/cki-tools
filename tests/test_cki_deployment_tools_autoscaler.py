"""Test cki.deployment_tools.autoscaler."""
import unittest

from cki_lib import config_tree

from cki.deployment_tools.autoscaler import Application


class TestPodsAutoscalerEvaluate(unittest.TestCase):
    """Test PodsAutoscaler."""

    def setUp(self):
        """Set up Application."""
        config = {
            '.default': {
                'namespace': 'test',
                'messages_per_replica': 20,
                'max_scale_up_step': 2,
                'max_scale_down_step': 2,
                'replicas_min': 1,
                'replicas_max': 5,
            },
            'test_app': {
                'queue': 'test_queue',
                'deployment_config': 'test_dc',
            }
        }
        config = config_tree.process_config_tree(config)
        self.app = Application('test_app', config['test_app'])

    def test_scale_up(self):
        """Test scaling up."""
        # No messages, no scaling up.
        self.assertEqual(1, self.app.evaluate(message_count=0, consumer_count=1))

        # Some messages but below messages_per_replica.
        self.assertEqual(1, self.app.evaluate(message_count=19, consumer_count=1))

        # Some messages but exactly messages_per_replica.
        self.assertEqual(1, self.app.evaluate(message_count=20, consumer_count=1))

        # Few more messages than messages_per_replica, replica_count should only increase by 1.
        self.assertEqual(2, self.app.evaluate(message_count=21, consumer_count=1))

        # More messages than messages_per_replica, replica_count increases by 2.
        self.assertEqual(3, self.app.evaluate(message_count=41, consumer_count=1))

        # More messages, replica_count increases by 2 only (max_scale_up_step).
        self.assertEqual(3, self.app.evaluate(message_count=200, consumer_count=1))
        self.assertEqual(5, self.app.evaluate(message_count=200, consumer_count=3))

        # More messages, replicas_max reached max.
        self.assertEqual(5, self.app.evaluate(message_count=2000, consumer_count=4))
        self.assertEqual(5, self.app.evaluate(message_count=2000, consumer_count=5))

    def test_scale_down(self):
        """Test scaling down."""
        # No messages, no scaling down (replicas_min)
        self.assertEqual(1, self.app.evaluate(message_count=0, consumer_count=1))

        # Some messages but exactly messages_per_replica.
        self.assertEqual(2, self.app.evaluate(message_count=40, consumer_count=2))

        # Some messages but below messages_per_replica.
        self.assertEqual(2, self.app.evaluate(message_count=39, consumer_count=2))

        # Few less messages than messages_per_replica, replica_count should only decrease by 1.
        self.assertEqual(2, self.app.evaluate(message_count=39, consumer_count=3))

        # Less messages than messages_per_replica, replica_count decreases by 2.
        self.assertEqual(1, self.app.evaluate(message_count=19, consumer_count=3))

        # Less messages, replica_count decreases by 2 only (max_scale_down_step).
        self.assertEqual(3, self.app.evaluate(message_count=0, consumer_count=5))

        # Less messages, replica_count reached min.
        self.assertEqual(1, self.app.evaluate(message_count=0, consumer_count=2))
        self.assertEqual(1, self.app.evaluate(message_count=0, consumer_count=3))
