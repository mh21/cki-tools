"""Beaker to KCIDB tests."""
import json
import os
import pathlib
import tempfile
import unittest
from unittest import mock
import xml.etree.ElementTree as ET

from cki.kcidb import beaker_to_kcidb

ASSETS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'assets')


class TestBeakerToKCIDB(unittest.TestCase):
    """Test beaker_to_kcidb module."""

    def test_get_param(self):
        """Test get_param."""
        task_xml = (
           '<task name="test" role="STANDALONE">'
           '  <fetch url="url"/>'
           '  <params>'
           '    <param name="CKI_NAME" value="kselftests upstream - tc"/>'
           '  </params>'
           '</task>'
        )

        cases = (
            ('Existing parameter', 'CKI_NAME', 'kselftests upstream - tc'),
            ('Non existing parameter', 'SOMETHING_ELSE', None)
        )
        task = ET.fromstring(task_xml)

        for description, key, value in cases:
            with self.subTest(description):
                self.assertEqual(value, beaker_to_kcidb.get_param(task, key))

    def test_is_task_a_cki_test(self):
        """ Test is_task_a_cki_test."""
        # Old logic
        cki_task_with_old_logic = (
           '<task name="test" role="STANDALONE">'
           '  <fetch url="url"/>'
           '  <params>'
           '    <param name="CKI_ID" value="5"/>'
           '  </params>'
           '</task>'
        )

        non_cki_task_with_old_logic = (
           '<task name="test" role="STANDALONE">'
           '  <fetch url="url"/>'
           '  <params/>'
           '</task>'
        )
        cki_task_with_new_logic = (
           '<task name="test" role="STANDALONE">'
           '  <fetch url="url"/>'
           '  <params>'
           '    <param name="CKI_IS_TEST" value="True"/>'
           '    <param name="CKI_ID" value="5"/>'
           '  </params>'
           '</task>'
        )

        non_cki_task_with_new_logic = (
           '<task name="test" role="STANDALONE">'
           '  <fetch url="url"/>'
           '  <params>'
           '    <param name="CKI_IS_TEST" value="False"/>'
           '    <param name="CKI_ID" value="5"/>'
           '  </params>'
           '</task>'
        )
        cases = (
            ('CKI TEST using old logic', cki_task_with_old_logic, True),
            ('NON CKI TEST using old logic', non_cki_task_with_old_logic, False),
            ('CKI TEST using new logic', cki_task_with_new_logic, True),
            ('NON CKI TEST using new logic', non_cki_task_with_new_logic, False),
        )

        for description, task_xml, expected in cases:
            with self.subTest(description):
                task = ET.fromstring(task_xml)
                self.assertEqual(expected, beaker_to_kcidb.is_task_a_cki_test(task))

    def test_xml_to_kcidb(self):
        """Test xml_to_kcidb."""
        beaker_xml_path = pathlib.Path(ASSETS_DIR, 'beaker_to_kcidb.xml')
        tests = beaker_to_kcidb.xml_to_kcidb(beaker_xml_path, 'redhat:build_id')
        self.assertEqual(
            [
                {
                    'build_id': 'redhat:build_id',
                    'comment': 'Boot test',
                    'id': 'redhat:build_id_upt_1',
                    'origin': 'redhat',
                    'path': 'boot_test',
                    'waived': False,
                },
                {
                    'build_id': 'redhat:build_id',
                    'comment': 'Reboot test',
                    'id': 'redhat:build_id_upt_2',
                    'origin': 'redhat',
                    'path': 'reboot_test',
                    'waived': False,
                },
                {
                    'build_id': 'redhat:build_id',
                    'comment': 'kselftests upstream - net',
                    'id': 'redhat:build_id_upt_3',
                    'origin': 'redhat',
                    'path': 'kselftest.upstream-net',
                    'waived': True,
                }
            ],
            tests
        )

    @mock.patch('cki.kcidb.beaker_to_kcidb.xml_to_kcidb')
    def test_main(self, xml_to_kcidb):
        """Test main."""
        kcidb_base = {'version': {'major': 4, 'minor': 0}}
        tests = [{'id': 'redhat:123', 'origin': 'redhat', 'build_id': 'redhat:456'}]
        xml_to_kcidb.return_value = tests
        beaker_xml_path = pathlib.Path(ASSETS_DIR, 'beaker_to_kcidb.xml')

        with tempfile.NamedTemporaryFile() as kcidb_file:
            kcidb_file = pathlib.Path(kcidb_file.name)
            kcidb_file.write_text(json.dumps(kcidb_base))

            argv = [
                '--beaker-xml', str(beaker_xml_path),
                '--kcidb-file', str(kcidb_file),
                '--build-id', 'redhat:build_id'
            ]
            beaker_to_kcidb.main(argv)

            self.assertEqual(
                {**kcidb_base, 'tests': tests},
                json.loads(kcidb_file.read_text())
            )
