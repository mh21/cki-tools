"""Test for cki.cki_tools.kpet_db_checker module."""
import io
import itertools
import pathlib
import tempfile
import unittest
from unittest import mock

from cki.cki_tools import kpet_db_checker

TASK_WITH_PARAMS = """
    <task name="foo">
    some lines before
    .....
      <param name="PARAM_A" value="True"/>
      <param value="True" name="PARAM_B" />
    ....
    some lines after
    </task>
"""

TASK_WITH_INVALID_PARAMS = """
    <task name="bar">
    some lines before
    .....
      <param name="PARAM_A"/>
      <param name="PARAM_B" value="True">
    ....
    some lines after
    </task>
"""

TASK_WITHOUT_PARAMS = """
    <task name="foobar">
    Lorem_ipsum
    More lorem_ipsum
    </task>
"""

INVALID_TASK = """
    <task name="something">
    fdfd
    <task>
"""

UNCLOSED_TASK = """
    <task name="something">
    fdfd
"""

NO_TASK = """
    Lorem ipsum
    Lorem ipsum
"""

FILE = (TASK_WITH_PARAMS + UNCLOSED_TASK + TASK_WITH_INVALID_PARAMS
        + TASK_WITHOUT_PARAMS + INVALID_TASK)

FILE_NO_TASK = NO_TASK

FILE_OK = NO_TASK + TASK_WITH_PARAMS + NO_TASK + TASK_WITH_PARAMS


class TestFunctions(unittest.TestCase):
    """Test functions."""

    def test_param(self):
        """Search a param in a block."""
        cases = (
            ('valid params - Searching PARAM_A', TASK_WITH_PARAMS, 'PARAM_A', True),
            ('valid params - Searching PARAM_B', TASK_WITH_PARAMS, 'PARAM_B', True),
            ('invalid params - Searching PARAM_A', TASK_WITH_INVALID_PARAMS, 'PARAM_A', False),
            ('invalid params - Searching PARAM_B', TASK_WITH_INVALID_PARAMS, 'PARAM_B', False),
            ('Without params ', TASK_WITHOUT_PARAMS, 'PARAM_B', False),
        )

        for description, block, param, expected in cases:
            with self.subTest(description):
                self.assertEqual(expected, kpet_db_checker.search_param(block, param))

    def test_get_tasks(self):
        """Method to search task in a open file."""
        tasks = kpet_db_checker.get_tasks(FILE.splitlines())
        self.assertEqual(3, len(tasks))
        self.assertEqual(2, tasks[0].start)
        self.assertEqual(9, tasks[0].end)
        self.assertEqual(14, tasks[1].start)
        self.assertEqual(21, tasks[1].end)
        self.assertEqual(23, tasks[2].start)
        self.assertEqual(26, tasks[2].end)

        self.assertEqual([], kpet_db_checker.get_tasks(FILE_NO_TASK.splitlines()))


class TestCLI(unittest.TestCase):
    """Test CLI."""

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_command_without_input_file(self, std_mock):
        """Test without input file."""
        expected_msg = 'error: the following arguments are required: --input-file'
        args = (
            '--param foo'
        ).split()
        with self.assertRaises(SystemExit) as context:
            kpet_db_checker.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_msg, std_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_command_with_invalid_file(self, std_mock):
        """Test without parama."""
        expected_msg = "No such file or directory: 'foo'"
        args = (
            '--input-file foo '
            '--param bar '
        ).split()
        with self.assertRaises(SystemExit) as context:
            kpet_db_checker.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_msg, std_mock.getvalue())

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_command_without_param(self, std_mock):
        """Test without param."""
        expected_msg = 'error: the following arguments are required: --param'
        with tempfile.TemporaryDirectory() as tmp_dir:
            file = pathlib.Path(tmp_dir, 'some_file')
            file.touch()
            args = (
                f'--input {file.resolve()} '
            ).split()
            with self.assertRaises(SystemExit) as context:
                kpet_db_checker.main(args)
        self.assertEqual('2', str(context.exception))
        self.assertIn(expected_msg, std_mock.getvalue())

    def test_search_with_errors(self):
        """Test Searches with errors."""
        expected_output = 1
        cases = (
            ('no param presents', 'Tasks: 3, errors: 3', ['FOO']),
            ('asking for one param found', 'Tasks: 3, errors: 2', ['PARAM_A']),
            ('asking for all params found', 'Tasks: 3, errors: 2', ['PARAM_A', 'PARAM_B']),
            ('asking for one params found and other not found',
             'Tasks: 3, errors: 3', ['PARAM_A', 'PARAM_C']),
        )
        with tempfile.TemporaryDirectory() as tmp_dir:
            file = pathlib.Path(tmp_dir, 'some_file')
            file.write_text(FILE, encoding='utf-8')
            for description, expected_msg, params in cases:
                with self.subTest(description), \
                        mock.patch('sys.stdout', new_callable=io.StringIO) as std_mock:
                    args = list(itertools.chain(
                        ['--input-file', file.as_posix()],
                        *(['--param', param] for param in params)))
                    output = kpet_db_checker.main(args)
                    self.assertEqual(expected_output, output)
                    self.assertIn(expected_msg, std_mock.getvalue())

    @mock.patch('sys.stdout', new_callable=io.StringIO)
    def test_search_without_errors(self, std_mock):
        """Test search without errors."""
        expected_output = 0
        expected_msg = 'Tasks: 2, errors: 0'
        with tempfile.TemporaryDirectory() as tmp_dir:
            file = pathlib.Path(tmp_dir, 'some_file')
            file.write_text(FILE_OK, encoding='utf-8')
            args = (
                f'--input-file {file.resolve()} '
                '--param PARAM_A '
                '--param PARAM_B '
            ).split()
            output = kpet_db_checker.main(args)
            self.assertEqual(expected_output, output)
            self.assertIn(expected_msg, std_mock.getvalue())
