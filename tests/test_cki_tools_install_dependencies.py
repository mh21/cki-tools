"""Tests for install_dependencies."""

import contextlib
import io
import subprocess
import typing
import unittest
from unittest import mock
import urllib

from cki.cki_tools import install_dependencies


class TestInstallDependencies(unittest.TestCase):
    """Tests for install_dependencies."""

    def __init__(self, *args: typing.Any, **kwargs: typing.Any) -> None:
        super().__init__(*args, **kwargs)
        self._mocked_runs: typing.List[
            typing.Tuple[typing.List[str], int, typing.Optional[str]]] = []
        self._mocked_run_calls: typing.List[typing.List[str]] = []
        self._mocked_urls: typing.Dict[str, typing.Tuple[int, typing.Optional[bytes]]] = {}

    def _mock_run(
        self,
        args: typing.List[str],
        *,
        check: bool = False,
        **_: typing.Any,
    ) -> typing.Any:
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_run_calls.append(args)
        if check and returncode:
            raise subprocess.CalledProcessError(returncode, args, output=stdout)
        return subprocess.CompletedProcess(args, returncode, stdout=stdout)

    def _add_run_result(
        self,
        args: typing.List[str],
        returncode: int,
        stdout: typing.Optional[str] = None
    ) -> None:
        self._mocked_runs.append((args, returncode, stdout))

    def _mock_urlopen(self, url: str) -> typing.Any:
        mocked = self._mocked_urls.get(url)
        if not mocked:
            self.fail(f'URL {url} not found in mocked urllib.request.urlopen')
        (status, body) = mocked
        if status != 200:
            raise urllib.error.HTTPError(url, status, 'not found', {}, None)
        result = mock.MagicMock()
        result.__enter__.return_value.read.return_value = body
        return result

    def _add_url(
        self,
        url: str,
        status: int = 200,
        body: typing.Optional[bytes] = None
    ) -> None:
        self._mocked_urls[url] = (status, body)

    def _dep(
        self,
        dependency: str,
        name: typing.Optional[str] = None,
        version: typing.Optional[str] = None,
        extras: str = '',
        url: typing.Optional[str] = None,
        invalid: bool = False,
    ) -> None:
        # pylint: disable=too-many-arguments,protected-access
        if invalid:
            self.assertRaises(Exception,
                              install_dependencies.DnfResolver._parse_dependency, dependency)
        else:
            self.assertEqual(
                install_dependencies.DnfResolver._parse_dependency(dependency),
                install_dependencies.Dependency(name, version, extras, url))

    def test_parse_dependencies(self) -> None:
        """Test dependency parsing."""
        self._dep('anymarkup',
                  'anymarkup')
        self._dep('beautifulsoup4',
                  'beautifulsoup4')
        self._dep('cached-property',
                  'cached-property')
        self._dep('cached_property',
                  'cached-property')
        self._dep('cki-lib @ git+https://gitlab.com/cki-project/cki-lib.git/',
                  'cki-lib', None, '', 'git+https://gitlab.com/cki-project/cki-lib.git/')
        self._dep('cki_lib @ git+https://gitlab.com/cki-project/cki-lib.git',
                  'cki-lib', None, '', 'git+https://gitlab.com/cki-project/cki-lib.git')
        self._dep('cki-lib[psql] @ git+https://gitlab.com/cki-project/cki-lib.git/',
                  'cki-lib', None, 'psql', 'git+https://gitlab.com/cki-project/cki-lib.git/')
        self._dep('cki-tools[brew] @ git+https://gitlab.com/cki-project/cki-tools.git/',
                  'cki-tools', None, 'brew', 'git+https://gitlab.com/cki-project/cki-tools.git/')
        self._dep('GitPython',
                  'gitpython')
        self._dep('gql >= 3.0.0a6',
                  'gql', '>= 3.0.0a6')
        self._dep('kcidb @ git+https://github.com/kernelci/kcidb@353686dc4f2a',
                  'kcidb', None, '', 'git+https://github.com/kernelci/kcidb@353686dc4f2a')
        self._dep('kcidb-io @ git+https://github.com/kernelci/kcidb-io.git@v3',
                  'kcidb-io', None, '', 'git+https://github.com/kernelci/kcidb-io.git@v3')
        self._dep('psycopg2-binary==2.8.6',
                  'psycopg2-binary', '==2.8.6')
        self._dep('sentry_sdk',
                  'sentry-sdk')
        self._dep('sentry-sdk[flask]',
                  'sentry-sdk', None, 'flask')
        self._dep('stomp.py',
                  'stomp.py')
        self._dep('unsupported @ stomp.py',
                  invalid=True)
        self._dep('kcidb-io @ git+https://github.com/kernelci/kcidb-io.git@v3 < 1',
                  invalid=True)

    def test_dependencies(self) -> None:
        """Test basic dependency resolution and installation."""
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(anymarkup)'], 0,
                             'python3-anymarkup-0:0.8.1-7.fc35.noarch')
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(unknown-dep)'], 0,
                             '')
        self._add_run_result(['dnf', 'install', '--assumeyes', '--', 'python3dist(anymarkup)'], 0)
        resolver = install_dependencies.DnfResolver(recursive=True)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            resolver.resolve('[options]\ninstall_requires=anymarkup\n unknown-dep', [], [])
            self.assertCountEqual(resolver.pip_via_distribution.values(),
                                  ['python3dist(anymarkup)'])
            self.assertCountEqual(resolver.pip_unavailable, [install_dependencies.Dependency(
                name='unknown-dep', version=None, extras_string='', url=None)])

            resolver.install_via_distribution()
            self.assertEqual(self._mocked_run_calls[-1][1], 'install')

        status_output = io.StringIO()
        with contextlib.redirect_stdout(status_output):
            resolver.status()
        self.assertEqual(status_output.getvalue(),
                         'Installation via dnf:\n' +
                         '  anymarkup\n' +
                         'Installation via pip:\n' +
                         '  unknown-dep\n')

    def test_extra_package_dependencies(self) -> None:
        """Test extras for dnf packages."""
        self._add_run_result(['dnf', 'repoquery', '--whatprovides',
                              r'python3dist(anymarkup\[foo,bar\])'], 0,
                             'python3-anymarkup-0:0.8.1-7.fc35.noarch')
        resolver = install_dependencies.DnfResolver(recursive=True)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            resolver.resolve('[options]\ninstall_requires=anymarkup[foo,bar]', [], [])
            self.assertCountEqual(resolver.pip_via_distribution.values(),
                                  [r'python3dist(anymarkup\[foo,bar\])'])

    def test_extras(self) -> None:
        """Test parsing extras."""
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(anymarkup)'], 0,
                             'python3-anymarkup-0:0.8.1-7.fc35.noarch')
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(another-dep)'], 0,
                             'python3-another-dep-0:0.8.1-7.fc35.noarch')
        resolver = install_dependencies.DnfResolver(recursive=True)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            resolver.resolve('[options]\ninstall_requires=anymarkup\n' +
                             '[options.extras_require]\ndev=another-dep', ['dev'], [])
            self.assertCountEqual(resolver.pip_via_distribution.values(),
                                  ['python3dist(anymarkup)', 'python3dist(another-dep)'])

    def test_environment_extras(self) -> None:
        """Test parsing extras from environments."""
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(anymarkup)'], 0,
                             'python3-anymarkup-0:0.8.1-7.fc35.noarch')
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(another-dep)'], 0,
                             'python3-another-dep-0:0.8.1-7.fc35.noarch')
        resolver = install_dependencies.DnfResolver(recursive=True)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            resolver.resolve('[options]\ninstall_requires=anymarkup\n' +
                             '[options.extras_require]\ndev=another-dep\n' +
                             '[testenv]\nextras=dev', [], ['testenv'])
            self.assertCountEqual(resolver.pip_via_distribution.values(),
                                  ['python3dist(anymarkup)', 'python3dist(another-dep)'])

    def test_environment_deps(self) -> None:
        """Test parsing deps from environments."""
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(anymarkup)'], 0,
                             'python3-anymarkup-0:0.8.1-7.fc35.noarch')
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(another-dep)'], 0,
                             'python3-another-dep-0:0.8.1-7.fc35.noarch')
        resolver = install_dependencies.DnfResolver(recursive=True)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            resolver.resolve('[options]\ninstall_requires=anymarkup\n' +
                             '[testenv]\ndeps=another-dep', [], ['testenv'])
            self.assertCountEqual(resolver.pip_via_distribution.values(),
                                  ['python3dist(anymarkup)', 'python3dist(another-dep)'])

    def test_comments(self) -> None:
        """Test ignoring comments."""
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(anymarkup)'], 0,
                             'python3-anymarkup-0:0.8.1-7.fc35.noarch')
        resolver = install_dependencies.DnfResolver(recursive=True)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            resolver.resolve('[options]\ninstall_requires=anymarkup  # comment', [], [])
            self.assertCountEqual(resolver.pip_via_distribution.values(),
                                  ['python3dist(anymarkup)'])

    def test_gitlab_dep_urls(self) -> None:
        """Test parsing gitlab url dependencies."""
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(anymarkup)'], 0,
                             'python3-anymarkup-0:0.8.1-7.fc35.noarch')
        self._add_url(
            'https://gitlab.com/path/-/raw/HEAD/setup.cfg',
            body=b'[options]\ninstall_requires=anymarkup\n', status=200)
        resolver = install_dependencies.DnfResolver(recursive=True)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run), \
                mock.patch('urllib.request.urlopen', autospec=True, side_effect=self._mock_urlopen):
            resolver.resolve(
                '[options]\ninstall_requires=pkg @ git+https://gitlab.com/path.git', [], [])
            self.assertCountEqual(resolver.pip_via_distribution.values(),
                                  ['python3dist(anymarkup)'])

    def test_dep_branch_urls(self) -> None:
        """Test parsing branch names from url dependencies."""
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(anymarkup)'], 0,
                             'python3-anymarkup-0:0.8.1-7.fc35.noarch')
        self._add_url(
            'https://gitlab.com/path/-/raw/main/setup.cfg',
            body=b'[options]\ninstall_requires=anymarkup\n', status=200)
        resolver = install_dependencies.DnfResolver(recursive=True)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run), \
                mock.patch('urllib.request.urlopen', autospec=True, side_effect=self._mock_urlopen):
            resolver.resolve(
                '[options]\ninstall_requires=pkg @ git+https://gitlab.com/path.git@main', [], [])
            self.assertCountEqual(resolver.pip_via_distribution.values(),
                                  ['python3dist(anymarkup)'])

    def test_dep_url_deduplication(self) -> None:
        """Test deduplication of recursive dependencies."""
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(anymarkup)'], 0,
                             'python3-anymarkup-0:0.8.1-7.fc35.noarch')
        self._add_url(
            'https://gitlab.com/path/-/raw/main/setup.cfg',
            body=b'[options]\ninstall_requires=anymarkup\n', status=200)
        resolver = install_dependencies.DnfResolver(recursive=True)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run), \
                mock.patch('urllib.request.urlopen', autospec=True, side_effect=self._mock_urlopen):
            resolver.resolve('[options]\ninstall_requires=anymarkup\n'
                             ' pkg @ git+https://gitlab.com/path.git@main', [], [])
            self.assertCountEqual(resolver.pip_via_distribution.values(),
                                  ['python3dist(anymarkup)'])

    def test_dep_url_py_fallabck(self) -> None:
        """Test fallback to setup.py for missing URL dependencies."""
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(anymarkup)'], 0,
                             'python3-anymarkup-0:0.8.1-7.fc35.noarch')
        self._add_url(
            'https://gitlab.com/path/-/raw/HEAD/setup.cfg', status=404)
        self._add_url(
            'https://gitlab.com/path/-/raw/HEAD/setup.py',
            body=b'setuptools.setup(install_requires=["anymarkup"])', status=200)
        resolver = install_dependencies.DnfResolver(recursive=True)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run), \
                mock.patch('urllib.request.urlopen', autospec=True, side_effect=self._mock_urlopen):
            resolver.resolve(
                '[options]\ninstall_requires=pkg @ git+https://gitlab.com/path.git', [], [])
            self.assertCountEqual(resolver.pip_via_distribution.values(),
                                  ['python3dist(anymarkup)'])

    def test_github_dep_urls(self) -> None:
        """Test parsing github url dependencies."""
        self._add_run_result(['dnf', 'repoquery', '--whatprovides', 'python3dist(anymarkup)'], 0,
                             'python3-anymarkup-0:0.8.1-7.fc35.noarch')
        self._add_url(
            'https://raw.githubusercontent.com/path/HEAD/setup.cfg',
            body=b'[options]\ninstall_requires=anymarkup\n', status=200)
        resolver = install_dependencies.DnfResolver(recursive=True)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run), \
                mock.patch('urllib.request.urlopen', autospec=True, side_effect=self._mock_urlopen):
            resolver.resolve(
                '[options]\ninstall_requires=pkg @ git+https://github.com/path.git', [], [])
            self.assertCountEqual(resolver.pip_via_distribution.values(),
                                  ['python3dist(anymarkup)'])
