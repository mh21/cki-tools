"""Datawarehouse Submitter tests."""
from contextlib import redirect_stdout
from io import StringIO
import json
import logging
import unittest

from gitlab.exceptions import GitlabGetError
import responses

from cki.kcidb.datawarehouse_submitter import LOGGER
from cki.kcidb.datawarehouse_submitter import get_kcidb_data
from cki.kcidb.datawarehouse_submitter import handle_output
from cki.kcidb.datawarehouse_submitter import process_single


class TestDatawarehouseSubmitter(unittest.TestCase):
    """Test cki.kcidb.datawarehouse_submitter."""

    def test_get_kcidb_data(self):
        """"Test get_kcidb_data"""

        job = unittest.mock.Mock()
        job.name = "Foobar"

        job.artifact.side_effect = GitlabGetError
        with self.subTest("Assert GitLab artifacts errors are handled"):
            with self.assertLogs(logger=LOGGER, level=logging.INFO) as log:
                result = get_kcidb_data(job)

            expected_log = (
                f"INFO:{LOGGER.name}:"
                f"Job {job.name} has no kcidb_all.json file"
            )
            self.assertIn(expected_log, log.output)
            self.assertIsNone(result)
            job.artifact.assert_called_once_with('kcidb_all.json')

        job.artifact.reset_mock(side_effect=True)
        job.artifact.return_value = ""
        with self.subTest("Assert an empty artifact is well handled"):
            with self.assertLogs(logger=LOGGER, level=logging.INFO) as log:
                result = get_kcidb_data(job)

            expected_log = (
                f"INFO:{LOGGER.name}:"
                f"Job {job.name} has an empty kcidb_all.json file"
            )
            self.assertIn(expected_log, log.output)
            self.assertIsNone(result)
            job.artifact.assert_called_once_with('kcidb_all.json')

        job.artifact.reset_mock()
        job.artifact.return_value = '{"data": "undecodable value",}'  # invalid comma
        with self.subTest("Assert that non json-decodable data are well handled"):
            with self.assertLogs(logger=LOGGER, level=logging.INFO) as log:
                result = get_kcidb_data(job)

            expected_log = (
                f"INFO:{LOGGER.name}:"
                f"Job {job.name} has an invalid kcidb_all.json file."
                " Expecting property name enclosed in double quotes:"
                " line 1 column 30 (char 29)"
            )
            self.assertIn(expected_log, log.output)
            self.assertIsNone(result)
            job.artifact.assert_called_once_with('kcidb_all.json')

        job.artifact.reset_mock(side_effect=True)
        expected_data = {"Mocked": "data"}
        job.artifact.return_value = json.dumps(expected_data)
        with self.subTest("Assert successful decoding returns properly"):
            result = get_kcidb_data(job)

            self.assertEqual(result, expected_data)
            job.artifact.assert_called_once_with('kcidb_all.json')

    @unittest.mock.patch("datawarehouse.Datawarehouse")
    def test_handle_output(self, mocked_dw):
        """Test handle_output"""
        # Prepare args to assert handle_output outputs to stdout
        args = unittest.mock.Mock(
            output_file=None,
            output_stdout=True,
            push=False,
            datawarehouse_url=None,
            datawarehouse_token=None,
        )
        data = {'version': {'major': 4, 'minor': 0}}

        with redirect_stdout(StringIO()) as mocked_stdout:

            with self.subTest("Assert it doesn't crash with an empty data payload"):
                handle_output(args, data=None)
                self.assertEqual(mocked_stdout.getvalue(), "",
                                 "Expect no output when data is None")
                mocked_dw.assert_not_called()

            with self.subTest("Assert output_stdout=True outputs as expected"):
                handle_output(args, data=data)
                self.assertEqual(
                    mocked_stdout.getvalue(),
                    "{\"version\": {\"major\": 4, \"minor\": 0}}\n"
                )
                mocked_dw.assert_not_called()

            # reset stream
            mocked_stdout.truncate(0)
            mocked_stdout.seek(0)

            args.push = True
            args.output_stdout = False

            with self.subTest("Assert push=True submits to DW"):
                handle_output(args, data=data)
                self.assertEqual(mocked_stdout.getvalue(), "",
                                 "Expect no output when output_stdout=False")
                mocked_dw.assert_called_once()
                mocked_dw().kcidb.submit.create.assert_called_once_with(data=data)

    @unittest.mock.patch("cki.kcidb.datawarehouse_submitter.handle_output")
    @responses.activate
    def test_process_single_job(self, mocked_handle_output):
        """Test process_single works as expected for a single job """
        gitlab_url = 'gitlab.com'
        project_url = f"https://{gitlab_url}/api/v4/projects/group_foo%2Fproject_foo"
        job_id = '1'

        responses.get(f'{project_url}/jobs/{job_id}', json={"id": job_id, "name": "Job name"})

        args = unittest.mock.Mock(
            gitlab_url=gitlab_url,
            project='group_foo/project_foo',
            id=job_id,
            kind='job',  # This is a key value in this test
        )

        data_examples = [
            None,  # Empty kcidb_all.json
            {"mocked kcidb data": 1234},
        ]

        for data in data_examples:
            with self.subTest(data=data):
                responses.get(f'{project_url}/jobs/{job_id}/artifacts/kcidb_all.json', json=data)

                process_single(args=args)

                mocked_handle_output.assert_called_once_with(args, data)
                mocked_handle_output.reset_mock()

    @unittest.mock.patch("cki.kcidb.datawarehouse_submitter.handle_output")
    @responses.activate
    def test_process_single_pipeline(self, mocked_handle_output):
        """Test process_single works as expected for all jobs in a pipeline """
        gitlab_url = 'gitlab.com'
        project_url = f"https://{gitlab_url}/api/v4/projects/group_foo%2Fproject_foo"
        pipeline_id = '9'

        job_ids = ['1', '2']
        jobs = []
        for job_id in job_ids:
            job_data = {"id": job_id, "name": f"Job #{job_id}"}
            jobs.append(job_data)
            responses.get(f'{project_url}/jobs/{job_id}', json=job_data)
            responses.get(f'{project_url}/jobs/{job_id}/artifacts/kcidb_all.json',
                          json={"mocked kcidb data": job_id})

        responses.get(f'{project_url}/pipelines/{pipeline_id}/jobs', json=jobs)

        args = unittest.mock.Mock(
            gitlab_url=gitlab_url,
            project='group_foo/project_foo',
            id=pipeline_id,
            kind='pipeline',  # This is a key value in this test
        )

        process_single(args=args)

        args_list = [args for args, kwargs in mocked_handle_output.call_args_list]
        self.assertEqual(
            args_list,
            [
                (args, {"mocked kcidb data": '1'}),
                (args, {"mocked kcidb data": '2'}),
            ]
        )
        mocked_handle_output.reset_mock()
