"""Tests for the scratch cleaner."""
import os
import pathlib
import tempfile
import unittest
from unittest import mock

import responses

from cki.cki_tools import scratch_cleaner


class TestScratchCleaner(unittest.TestCase):
    """Tests for the scratch cleaner."""

    def _test(self, status, *,
              removed=True,
              fail=False,
              ircbot=False):
        responses.add(responses.GET,
                      'https://host/api/v4/projects/1/jobs/2',
                      json={'status': status,
                            'id': 2,
                            'pipeline': {'id': 3},
                            'name': 'name',
                            'web_url': 'web_url',
                            'project_path': 'project_path'})
        responses.add(responses.POST,
                      'https://bot.url/',
                      status=200 if ircbot else 404)
        with tempfile.TemporaryDirectory() as directory:
            with mock.patch('shutil.rmtree') as rmtree:
                job_dir = pathlib.Path(f'{directory}/host/1/2')
                job_dir.mkdir(parents=True)
                (job_dir / 'file.txt').write_text('foo')
                if fail:
                    rmtree.side_effect = Exception()
                    with self.assertLogs(scratch_cleaner.LOGGER, 'ERROR'):
                        result = scratch_cleaner.main(['--scratch-volume', directory])
                    self.assertNotEqual(result, 0)
                elif removed:
                    result = scratch_cleaner.main(['--scratch-volume', directory])
                    rmtree.assert_called_with(job_dir)
                    if ircbot:
                        self.assertTrue(any(r.request.url == 'https://bot.url/'
                                            for r in responses.calls))
                else:
                    result = scratch_cleaner.main(['--scratch-volume', directory])
                    rmtree.assert_not_called()

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch.dict(os.environ, {'IRC_BOT_URL': 'bot.url'})
    def test_success(self):
        """Check that scratch trees for successful jobs are removed."""
        self._test('success')

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_canceled(self):
        """Check that scratch trees for canceled jobs are removed."""
        self._test('canceled')

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_failed(self):
        """Check that scratch trees for failed jobs are removed."""
        self._test('failed')

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_running(self):
        """Check that scratch trees for running jobs are not removed."""
        self._test('running', removed=False)

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_failed_non_prod(self):
        """Check that scratch trees for failed jobs are not removed in non-production."""
        self._test('failed', removed=False)

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_rmtree_fail(self):
        """Check that rmtree failures are reported."""
        self._test('failed', fail=True)

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch.dict(os.environ, {'IRCBOT_URL': 'https://bot.url'})
    def test_irc_notification_failure(self):
        """Check that cleaning is reported on IRC."""
        self._test('failed')

    @responses.activate
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch.dict(os.environ, {'IRCBOT_URL': 'https://bot.url'})
    def test_irc_notification(self):
        """Check that cleaning is reported on IRC."""
        self._test('failed', ircbot=True)
