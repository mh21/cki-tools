"""Chatbot message relaying tests."""
import os
import unittest
from unittest import mock

import responses

from cki_tools.slack_bot import slack


class TestBot(unittest.TestCase):
    """ Test message relay."""

    @responses.activate
    def test_send_message(self):
        """Test send_message."""

        webhook_url = 'https://hooks.slack.com/services/m0Ock1ng/w3b/h0Oks'
        message = "This is an example of a message 😊"

        expected_json = {
            "text": message,
            "unfurl_links": False,
            "unfurl_media": False,
        }
        expected_resp = responses.add(responses.POST, webhook_url, json=expected_json)

        with mock.patch.dict(os.environ, {
                'CKI_SLACK_BOT_WEBHOOK': webhook_url}):
            slack.send_message(message)

        self.assertEqual(expected_resp.call_count, 1)
