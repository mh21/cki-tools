"""Unit tests for the gitlab_ci_bot."""
import unittest

from cki.cki_tools import gitlab_ci_bot


class TestGitLabCiBot(unittest.TestCase):
    """Unit tests for the gitlab_ci_bot."""
    project_config = {
        'default_pipelines': ['cki'],
    }
    pipelines_config = {
        'cki': {'default_branches': ['rhel8', 'rhel7', 'upstream-stable']},
        'brew': {'default_branches': ['rhel8', 'rhel7']},
    }

    def test_group_default(self):
        """Request the defaults groups."""
        self.check_pipeline_request('', set())

    def test_group_all(self):
        """Request the all groups."""
        self.check_pipeline_request('[all]', {
            ('cki', 'rhel7'),
            ('cki', 'rhel8'),
            ('cki', 'upstream-stable'),
        })

    def test_group_another(self):
        """Request a specific group."""
        self.check_pipeline_request('[brew]', {
            ('brew', 'rhel7'),
            ('brew', 'rhel8'),
        })

    def test_group_backslashes(self):
        """Check that backslashes in groups are ignored."""
        self.check_pipeline_request(r'\[brew\]', {
            ('brew', 'rhel7'),
            ('brew', 'rhel8'),
        })

    def test_group_no_brackets(self):
        """Check that both brackets are required."""
        for what in ('brew', '[brew', 'brew]'):
            self.check_pipeline_request(what, set())

    def test_group_multiple(self):
        """Request multiple groups."""
        self.check_pipeline_request('[cki][brew]', {
            ('cki', 'rhel7'),
            ('cki', 'rhel8'),
            ('cki', 'upstream-stable'),
            ('brew', 'rhel7'),
            ('brew', 'rhel8'),
        })

    def test_branch_single(self):
        """Request a single branch."""
        self.check_pipeline_request('[cki/rhel8]', {
            ('cki', 'rhel8'),
        })

    def test_branch_single_no_default(self):
        """Request a single branch not in the defaults."""
        self.check_pipeline_request('[cki/kernel-rt-rhel8]', {
            ('cki', 'kernel-rt-rhel8'),
        })

    def test_branch_single_upstream(self):
        """Request a single branch with a dash."""
        self.check_pipeline_request('[cki/upstream-stable]', {
            ('cki', 'upstream-stable'),
        })

    def test_branch_multiple(self):
        """Request branches from different groups."""
        self.check_pipeline_request('[cki/rhel8][brew/rhel8]', {
            ('cki', 'rhel8'),
            ('brew', 'rhel8'),
        })

    def test_branch_only_backslashes(self):
        """Check that backslashes for a command are ignored."""
        self.check_pipeline_request(r'\[cki/rhel8\]', {
            ('cki', 'rhel8'),
        })

    def test_variable_simple(self):
        """Check that specifying variables works."""
        self._check_variable(r'[key=value]', {'key': 'value'})

    def test_variable_backslashes(self):
        """Check that backslashes when specifying variables are ignored."""
        self._check_variable(r'\[key=value\]', {'key': 'value'})

    def test_variable_slash(self):
        """Check that variables with slashes work."""
        self._check_variable(r'[key/slash=value/slash]',
                             {'key/slash': 'value/slash'})

    def test_variable_square_bracket(self):
        """Check that variables with an escaped closing square bracket work."""
        self._check_variable(r'[key=square\u005Dbracket]',
                             {'key': 'square]bracket'})

    def check_pipeline_request(self, what, pipelines):
        """Request pipelines and verify the correct branches are used."""
        commands = gitlab_ci_bot.parse_commands(what)
        branches = gitlab_ci_bot.tested_branches(self.pipelines_config,
                                                 self.project_config, commands)
        self.assertEqual(branches, pipelines)

    def _check_variable(self, what, expected_value):
        commands = gitlab_ci_bot.parse_commands(what)
        self.assertEqual(commands['variables'], expected_value)
